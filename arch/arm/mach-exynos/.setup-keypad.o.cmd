cmd_arch/arm/mach-exynos/setup-keypad.o := /mnt/data/git/dewi/EM4412-android4.4.4/prebuilts/gcc/linux-x86/arm/arm-eabi-4.7/bin/arm-eabi-gcc -Wp,-MD,arch/arm/mach-exynos/.setup-keypad.o.d  -nostdinc -isystem /mnt/data/git/dewi/EM4412-android4.4.4/prebuilts/gcc/linux-x86/arm/arm-eabi-4.7/bin/../lib/gcc/arm-eabi/4.7/include -I/mnt/data/git/dewi/EM4412-android4.4.4/kernel/arch/arm/include -Iarch/arm/include/generated -Iinclude  -include include/generated/autoconf.h -D__KERNEL__ -mlittle-endian -Iarch/arm/mach-exynos/include -Iarch/arm/plat-s5p/include -Iarch/arm/plat-samsung/include -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -fno-strict-aliasing -fno-common -Werror-implicit-function-declaration -Wno-format-security -fno-delete-null-pointer-checks -O2 -marm -fno-dwarf2-cfi-asm -fno-omit-frame-pointer -mapcs -mno-sched-prolog -mabi=aapcs-linux -mno-thumb-interwork -D__LINUX_ARM_ARCH__=7 -march=armv7-a -msoft-float -Uarm -Wframe-larger-than=1024 -fno-stack-protector -Wno-unused-but-set-variable -fno-omit-frame-pointer -fno-optimize-sibling-calls -Wdeclaration-after-statement -Wno-pointer-sign -fno-strict-overflow -fconserve-stack -DCC_HAVE_ASM_GOTO    -D"KBUILD_STR(s)=\#s" -D"KBUILD_BASENAME=KBUILD_STR(setup_keypad)"  -D"KBUILD_MODNAME=KBUILD_STR(setup_keypad)" -c -o arch/arm/mach-exynos/setup-keypad.o arch/arm/mach-exynos/setup-keypad.c

source_arch/arm/mach-exynos/setup-keypad.o := arch/arm/mach-exynos/setup-keypad.c

deps_arch/arm/mach-exynos/setup-keypad.o := \
  include/linux/gpio.h \
    $(wildcard include/config/generic/gpio.h) \
  /mnt/data/git/dewi/EM4412-android4.4.4/kernel/arch/arm/include/asm/gpio.h \
  arch/arm/mach-exynos/include/mach/gpio.h \
    $(wildcard include/config/arch/exynos4.h) \
    $(wildcard include/config/samsung/gpio/extra.h) \
    $(wildcard include/config/arch/exynos5.h) \
  arch/arm/mach-exynos/include/mach/gpio-exynos4.h \
    $(wildcard include/config/s3c/gpio/space.h) \
  arch/arm/mach-exynos/include/mach/gpio-exynos5.h \
  include/asm-generic/gpio.h \
    $(wildcard include/config/gpiolib.h) \
    $(wildcard include/config/of/gpio.h) \
    $(wildcard include/config/of.h) \
    $(wildcard include/config/gpio/sysfs.h) \
  include/linux/kernel.h \
    $(wildcard include/config/lbdaf.h) \
    $(wildcard include/config/preempt/voluntary.h) \
    $(wildcard include/config/debug/spinlock/sleep.h) \
    $(wildcard include/config/prove/locking.h) \
    $(wildcard include/config/ring/buffer.h) \
    $(wildcard include/config/tracing.h) \
    $(wildcard include/config/numa.h) \
    $(wildcard include/config/compaction.h) \
    $(wildcard include/config/ftrace/mcount/record.h) \
  /mnt/data/git/dewi/EM4412-android4.4.4/prebuilts/gcc/linux-x86/arm/arm-eabi-4.7/bin/../lib/gcc/arm-eabi/4.7/include/stdarg.h \
  include/linux/linkage.h \
  include/linux/compiler.h \
    $(wildcard include/config/sparse/rcu/pointer.h) \
    $(wildcard include/config/trace/branch/profiling.h) \
    $(wildcard include/config/profile/all/branches.h) \
    $(wildcard include/config/enable/must/check.h) \
    $(wildcard include/config/enable/warn/deprecated.h) \
  include/linux/compiler-gcc.h \
    $(wildcard include/config/arch/supports/optimized/inlining.h) \
    $(wildcard include/config/optimize/inlining.h) \
  include/linux/compiler-gcc4.h \
  /mnt/data/git/dewi/EM4412-android4.4.4/kernel/arch/arm/include/asm/linkage.h \
  include/linux/stddef.h \
  include/linux/types.h \
    $(wildcard include/config/uid16.h) \
    $(wildcard include/config/arch/dma/addr/t/64bit.h) \
    $(wildcard include/config/phys/addr/t/64bit.h) \
    $(wildcard include/config/64bit.h) \
  /mnt/data/git/dewi/EM4412-android4.4.4/kernel/arch/arm/include/asm/types.h \
  include/asm-generic/int-ll64.h \
  /mnt/data/git/dewi/EM4412-android4.4.4/kernel/arch/arm/include/asm/bitsperlong.h \
  include/asm-generic/bitsperlong.h \
  include/linux/posix_types.h \
  /mnt/data/git/dewi/EM4412-android4.4.4/kernel/arch/arm/include/asm/posix_types.h \
  include/linux/bitops.h \
  /mnt/data/git/dewi/EM4412-android4.4.4/kernel/arch/arm/include/asm/bitops.h \
    $(wildcard include/config/smp.h) \
  /mnt/data/git/dewi/EM4412-android4.4.4/kernel/arch/arm/include/asm/system.h \
    $(wildcard include/config/function/graph/tracer.h) \
    $(wildcard include/config/cpu/32v6k.h) \
    $(wildcard include/config/cpu/xsc3.h) \
    $(wildcard include/config/cpu/fa526.h) \
    $(wildcard include/config/arch/has/barriers.h) \
    $(wildcard include/config/arm/dma/mem/bufferable.h) \
    $(wildcard include/config/cpu/sa1100.h) \
    $(wildcard include/config/cpu/sa110.h) \
    $(wildcard include/config/cpu/v6.h) \
  include/linux/irqflags.h \
    $(wildcard include/config/trace/irqflags.h) \
    $(wildcard include/config/irqsoff/tracer.h) \
    $(wildcard include/config/preempt/tracer.h) \
    $(wildcard include/config/trace/irqflags/support.h) \
  include/linux/typecheck.h \
  /mnt/data/git/dewi/EM4412-android4.4.4/kernel/arch/arm/include/asm/irqflags.h \
  /mnt/data/git/dewi/EM4412-android4.4.4/kernel/arch/arm/include/asm/ptrace.h \
    $(wildcard include/config/cpu/endian/be8.h) \
    $(wildcard include/config/arm/thumb.h) \
  /mnt/data/git/dewi/EM4412-android4.4.4/kernel/arch/arm/include/asm/hwcap.h \
  /mnt/data/git/dewi/EM4412-android4.4.4/kernel/arch/arm/include/asm/outercache.h \
    $(wildcard include/config/outer/cache/sync.h) \
    $(wildcard include/config/outer/cache.h) \
  include/asm-generic/cmpxchg-local.h \
  include/asm-generic/bitops/non-atomic.h \
  include/asm-generic/bitops/fls64.h \
  include/asm-generic/bitops/sched.h \
  include/asm-generic/bitops/hweight.h \
  include/asm-generic/bitops/arch_hweight.h \
  include/asm-generic/bitops/const_hweight.h \
  include/asm-generic/bitops/lock.h \
  include/asm-generic/bitops/le.h \
  /mnt/data/git/dewi/EM4412-android4.4.4/kernel/arch/arm/include/asm/byteorder.h \
  include/linux/byteorder/little_endian.h \
  include/linux/swab.h \
  /mnt/data/git/dewi/EM4412-android4.4.4/kernel/arch/arm/include/asm/swab.h \
  include/linux/byteorder/generic.h \
  include/linux/log2.h \
    $(wildcard include/config/arch/has/ilog2/u32.h) \
    $(wildcard include/config/arch/has/ilog2/u64.h) \
  include/linux/printk.h \
    $(wildcard include/config/printk.h) \
    $(wildcard include/config/dynamic/debug.h) \
  include/linux/init.h \
    $(wildcard include/config/modules.h) \
    $(wildcard include/config/hotplug.h) \
  include/linux/dynamic_debug.h \
  /mnt/data/git/dewi/EM4412-android4.4.4/kernel/arch/arm/include/asm/bug.h \
    $(wildcard include/config/bug.h) \
    $(wildcard include/config/debug/bugverbose.h) \
  include/asm-generic/bug.h \
    $(wildcard include/config/generic/bug.h) \
    $(wildcard include/config/generic/bug/relative/pointers.h) \
  /mnt/data/git/dewi/EM4412-android4.4.4/kernel/arch/arm/include/asm/div64.h \
  include/linux/errno.h \
  /mnt/data/git/dewi/EM4412-android4.4.4/kernel/arch/arm/include/asm/errno.h \
  include/asm-generic/errno.h \
  include/asm-generic/errno-base.h \
  arch/arm/plat-samsung/include/plat/gpio-cfg.h \
    $(wildcard include/config/s5p/gpio/int.h) \

arch/arm/mach-exynos/setup-keypad.o: $(deps_arch/arm/mach-exynos/setup-keypad.o)

$(deps_arch/arm/mach-exynos/setup-keypad.o):
