/*
 * (C) 2011-2013 by xboot.org
 * Author: jianjun jiang <8192542@qq.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation
 *
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/pwm.h>
#include <linux/platform_device.h>
#include <mach/hardware.h>
#include <asm/mach-types.h>
#include <linux/gpio.h>
#include <asm/gpio.h>
#include <asm/delay.h>
#include <linux/clk.h>
#include <plat/gpio-cfg.h>

static void __xkey_probe(void)
{
}

static void __xkey_remove(void)
{
}

static ssize_t xkey_key_read(struct device *dev, struct device_attribute *attr, char *buf)
{
	if(!strcmp(attr->attr.name, "key0"))
	{
		if(gpio_get_value(EXYNOS4_GPX1(0)))
			return strlcpy(buf, "1\n", 3);
		return strlcpy(buf, "0\n", 3);
	}
	else if(!strcmp(attr->attr.name, "key1"))
	{
		if(gpio_get_value(EXYNOS4_GPX1(1)))
			return strlcpy(buf, "1\n", 3);
		return strlcpy(buf, "0\n", 3);
	}

	return strlcpy(buf, "0\n", 3);
}

static ssize_t xkey_key_write(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	return count;
}

static DEVICE_ATTR(key0, 0666, xkey_key_read, xkey_key_write);
static DEVICE_ATTR(key1, 0666, xkey_key_read, xkey_key_write);

static struct attribute * xkey_sysfs_entries[] = {
	&dev_attr_key0.attr,
	&dev_attr_key1.attr,
	NULL,
};

static struct attribute_group xkey_attr_group = {
	.name	= NULL,
	.attrs	= xkey_sysfs_entries,
};

static int xkey_probe(struct platform_device *pdev)
{
	__xkey_probe();

	return sysfs_create_group(&pdev->dev.kobj, &xkey_attr_group);
}

static int xkey_remove(struct platform_device *pdev)
{
	__xkey_remove();

	sysfs_remove_group(&pdev->dev.kobj, &xkey_attr_group);
	return 0;
}

#ifdef CONFIG_PM
static int xkey_suspend(struct platform_device *pdev, pm_message_t state)
{
	return 0;
}

static int xkey_resume(struct platform_device *pdev)
{
	return 0;
}

#else
#define xkey_suspend	NULL
#define xkey_resume	NULL
#endif

static struct platform_driver xkey_driver = {
	.probe		= xkey_probe,
	.remove		= xkey_remove,
	.suspend	= xkey_suspend,
	.resume		= xkey_resume,
	.driver		= {
		.name	= "xkey",
	},
};

static struct platform_device xkey_device = {
	.name      = "xkey",
	.id        = -1,
};

static int __devinit xkey_init(void)
{
	int ret;

	printk("xkey interface driver\r\n");

	ret = platform_device_register(&xkey_device);
	if(ret)
		printk("failed to register xkey device\n");

	ret = platform_driver_register(&xkey_driver);
	if(ret)
		printk("failed to register xkey driver\n");

	return ret;
}

static void xkey_exit(void)
{
	platform_driver_unregister(&xkey_driver);
}

module_init(xkey_init);
module_exit(xkey_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("jianjun jiang <8192542@qq.com>");
MODULE_DESCRIPTION("xkey interface driver");
