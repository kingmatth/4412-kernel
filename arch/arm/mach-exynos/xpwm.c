/*
 * (C) 2011-2013 by xboot.org
 * Author: jianjun jiang <8192542@qq.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation
 *
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/pwm.h>
#include <linux/platform_device.h>
#include <mach/hardware.h>
#include <asm/mach-types.h>
#include <linux/gpio.h>
#include <asm/gpio.h>
#include <asm/delay.h>
#include <linux/clk.h>
#include <plat/gpio-cfg.h>

static struct pwm_device * pwm1;
static struct pwm_device * pwm2;
static struct pwm_device * pwm3;

static void __xpwm_probe(void)
{
	pwm1 = pwm_request(1, "PWM1");
	pwm2 = pwm_request(2, "PWM2");
	pwm3 = pwm_request(3, "PWM3");

	s3c_gpio_cfgpin(EXYNOS4_GPD0(1), S3C_GPIO_SFN(2));
	s3c_gpio_cfgpin(EXYNOS4_GPD0(2), S3C_GPIO_SFN(2));
	s3c_gpio_cfgpin(EXYNOS4_GPD0(3), S3C_GPIO_SFN(2));

	pwm_config(pwm1, 63760, 127520);
	pwm_config(pwm2, 63760, 127520);
	pwm_config(pwm3, 63760, 127520);

	pwm_disable(pwm1);
	pwm_disable(pwm2);
	pwm_disable(pwm3);
}

static void __xpwm_remove(void)
{
	pwm_free(pwm1);
	pwm_free(pwm2);
	pwm_free(pwm3);
}

static ssize_t xpwm_channel_read(struct device *dev, struct device_attribute *attr, char *buf)
{
	return strlcpy(buf, "\n", 3);
}

static ssize_t xpwm_channel_write(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	int duty_ns = 63760;
	int period_ns = 127520;

	if(sscanf(buf, "%d:%d", &duty_ns, &period_ns) == 2)
	{
		if(!strcmp(attr->attr.name, "channel1"))
		{
			pwm_config(pwm1, duty_ns, period_ns);
			if(period_ns != 0)
				pwm_enable(pwm1);
			else
				pwm_disable(pwm1);
		}
		else if(!strcmp(attr->attr.name, "channel2"))
		{
			pwm_config(pwm2, duty_ns, period_ns);
			if(period_ns != 0)
				pwm_enable(pwm2);
			else
				pwm_disable(pwm2);
		}
		else if(!strcmp(attr->attr.name, "channel3"))
		{
			pwm_config(pwm3, duty_ns, period_ns);
			if(period_ns != 0)
				pwm_enable(pwm3);
			else
				pwm_disable(pwm3);
		}
	}

	return count;
}

static ssize_t xpwm_enable_read(struct device *dev, struct device_attribute *attr, char *buf)
{
	return strlcpy(buf, "\n", 3);
}

static ssize_t xpwm_enable_write(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	unsigned long enable = simple_strtoul(buf, NULL, 10);

	if(!strcmp(attr->attr.name, "enable1"))
	{
		if(enable)
			pwm_enable(pwm1);
		else
			pwm_disable(pwm1);
	}
	else if(!strcmp(attr->attr.name, "enable2"))
	{
		if(enable)
			pwm_enable(pwm2);
		else
			pwm_disable(pwm2);
	}
	else if(!strcmp(attr->attr.name, "enable3"))
	{
		if(enable)
			pwm_enable(pwm3);
		else
			pwm_disable(pwm3);
	}

	return count;
}

static DEVICE_ATTR(channel1, 0666, xpwm_channel_read, xpwm_channel_write);
static DEVICE_ATTR(channel2, 0666, xpwm_channel_read, xpwm_channel_write);
static DEVICE_ATTR(channel3, 0666, xpwm_channel_read, xpwm_channel_write);
static DEVICE_ATTR(enable1, 0666, xpwm_enable_read, xpwm_enable_write);
static DEVICE_ATTR(enable2, 0666, xpwm_enable_read, xpwm_enable_write);
static DEVICE_ATTR(enable3, 0666, xpwm_enable_read, xpwm_enable_write);

static struct attribute * xpwm_sysfs_entries[] = {
	&dev_attr_channel1.attr,
	&dev_attr_channel2.attr,
	&dev_attr_channel3.attr,
	&dev_attr_enable1.attr,
	&dev_attr_enable2.attr,
	&dev_attr_enable3.attr,
	NULL,
};

static struct attribute_group xpwm_attr_group = {
	.name	= NULL,
	.attrs	= xpwm_sysfs_entries,
};

static int xpwm_probe(struct platform_device *pdev)
{
	__xpwm_probe();

	return sysfs_create_group(&pdev->dev.kobj, &xpwm_attr_group);
}

static int xpwm_remove(struct platform_device *pdev)
{
	__xpwm_remove();

	sysfs_remove_group(&pdev->dev.kobj, &xpwm_attr_group);
	return 0;
}

#ifdef CONFIG_PM
static int xpwm_suspend(struct platform_device *pdev, pm_message_t state)
{
	return 0;
}

static int xpwm_resume(struct platform_device *pdev)
{
	return 0;
}

#else
#define xpwm_suspend	NULL
#define xpwm_resume	NULL
#endif

static struct platform_driver xpwm_driver = {
	.probe		= xpwm_probe,
	.remove		= xpwm_remove,
	.suspend	= xpwm_suspend,
	.resume		= xpwm_resume,
	.driver		= {
		.name	= "xpwm",
	},
};

static struct platform_device xpwm_device = {
	.name      = "xpwm",
	.id        = -1,
};

static int __devinit xpwm_init(void)
{
	int ret;

	printk("xpwm interface driver\r\n");

	ret = platform_device_register(&xpwm_device);
	if(ret)
		printk("failed to register xpwm device\n");

	ret = platform_driver_register(&xpwm_driver);
	if(ret)
		printk("failed to register xpwm driver\n");

	return ret;
}

static void xpwm_exit(void)
{
	platform_driver_unregister(&xpwm_driver);
}

module_init(xpwm_init);
module_exit(xpwm_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("jianjun jiang <8192542@qq.com>");
MODULE_DESCRIPTION("pwm interface driver");
