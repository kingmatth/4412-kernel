/* linux/drivers/video/samsung/s3cfb_ek070tn93.c
 *
 * Copyright (c) 2010 Samsung Electronics Co., Ltd.
 *		http://www.samsung.com/
 *
 * LTE480 4.8" WVGA Landscape LCD module driver for the SMDK
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
*/

#include "s3cfb.h"

/* EK070TN93 */
static struct s3cfb_lcd wvga_s70 = {
		.type = 1, // 7" resistance
		.flags = 1,  
		.width = 800,
		.height = 480,
		.bpp = 32,
		.freq = 60,
		
		.timing = {
			.h_fp = 10,
			.h_bp = 78,
			.h_sw = 10,
			.v_fp = 30,
			.v_fpe = 1,
			.v_bp = 30,
			.v_bpe = 1,
			.v_sw = 2,
		},
        .polarity = {
                .rise_vclk = 0,
                .inv_hsync = 1,
                .inv_vsync = 1,
                .inv_vden = 0,
        },
        .init_ldi = NULL,
};

/* VS070CXN */
static struct s3cfb_lcd wsvga_w101 = {
	.type = 2,  // 10.1"  resistance
	.flags = 0,
	.width= 1024,
	.height = 600,
	.bpp = 32,
	.freq = 60,

	.timing = {
		.h_fp = 40,
		.h_bp = 40,
		.h_sw = 120,
		.v_fp = 10,
		.v_fpe = 1,
		.v_bp = 10,
		.v_bpe = 1,
		.v_sw = 12,
	},
	.polarity = {
		.rise_vclk = 0,
		.inv_hsync = 1,
		.inv_vsync = 1,
		.inv_vden = 0,
	},
};
static struct s3cfb_lcd wvga_gt70 = {
	.type = 3, // 7" capacitance
	.flags = 0,  
	.width = 800,
	.height = 480,
	//.p_width = 155, /*modify by boardcon 2014.2.26*/
	//.p_height = 93, /*modify by boardcon 2014.2.26*/
	.bpp = 32,
	.freq = 60,//63,

	.timing = {
				.h_fp	= 110,
				.h_bp	= 16,
				.h_sw	= 30,
				.v_fp	= 10,
				.v_fpe	= 1,
				.v_bp	= 10,
				.v_bpe	= 1,
				.v_sw	= 13,
	},
	 .polarity = {                
		 .rise_vclk = 0,   
		.inv_hsync = 1,
		.inv_vsync = 1,
		.inv_vden = 0,
	},
};

static struct s3cfb_lcd wsvga_gt101 = {
	.type = 4,  // gt 10.1" capacitance
	.flags = 0,
	.width = 1024,
	.height = 600,
	.bpp = 32,
	.freq = 60,//63,

	.timing = {
				.h_fp	= 110,
				.h_bp	= 16,
				.h_sw	= 30,
				.v_fp	= 10,
				.v_fpe	= 1,
				.v_bp	= 10,
				.v_bpe	= 1,
				.v_sw	= 13,
	},
	 .polarity = {                
		 .rise_vclk = 0,   
		.inv_hsync = 1,
		.inv_vsync = 1,
		.inv_vden = 0,
	},
};

static struct s3cfb_lcd wsvga_gs101 = {
	.type = 5,  //  10.1" capacitance  samsung_gslX680
	.flags = 0,
	.width = 1024,
	.height = 600,
	.bpp = 32,
	.freq = 60,//63,

	.timing = {
				.h_fp	= 110,
				.h_bp	= 16,
				.h_sw	= 30,
				.v_fp	= 10,
				.v_fpe	= 1,
				.v_bp	= 10,
				.v_bpe	= 1,
				.v_sw	= 13,
	},
	 .polarity = {                
		 .rise_vclk = 0,   
		.inv_hsync = 1,
		.inv_vsync = 1,
		.inv_vden = 0,
	},
};


static struct s3cfb_lcd wsvga_w104 = {
	.type = 6,  // 10.4" resistance
	.flags = 1,
	.width = 800,
        .height = 600,
        .bpp = 32,
        .freq = 60,

	.timing = {
				.h_fp	= 110,
				.h_bp	= 16,
				.h_sw	= 30,
				.v_fp	= 10,
				.v_fpe	= 1,
				.v_bp	= 10,
				.v_bpe	= 1,
				.v_sw	= 13,
	},
        .polarity = {
                .rise_vclk = 0,
                .inv_hsync = 1,
                .inv_vsync = 1,
                .inv_vden = 0,
        },
};
static struct s3cfb_lcd wsvga_h43 = {
		.type = 7, // 4.3" resistance
		.flags = 1,  
		.width = 480,
		.height = 272,
		.bpp = 32,
		.freq = 60,
		
		.timing = {
			.h_fp = 2,
			.h_bp = 20,
			.h_sw = 41,
			.v_fp = 2,
			.v_fpe = 1,
			.v_bp = 2,
			.v_bpe = 1,
			.v_sw = 10,
		},
        .polarity = {
                .rise_vclk = 0,
                .inv_hsync = 1,
                .inv_vsync = 1,
                .inv_vden = 0,
        },
        .init_ldi = NULL,
};

/* VGA-1024X768 */
static struct s3cfb_lcd vga_1024_768 = {
	.width	= 1024,
	.height	= 768,
	.bpp	= 32,
	.freq	= 60,

	.timing = {
		.h_fp	= 24,
		.h_bp	= 160,
		.h_sw	= 136,
		.v_fp	= 3,
		.v_fpe	= 1,
		.v_bp	= 29,
		.v_bpe	= 1,
		.v_sw	= 6,
	},

	.polarity = {
		.rise_vclk      = 1,
		.inv_hsync      = 0,
		.inv_vsync      = 0,
		.inv_vden       = 0,
	},
	
	.init_ldi = NULL,
};

/* VGA-1440X900 */
static struct s3cfb_lcd vga_1440_900 = {
	.width  = 1440,
	.height = 900,
	.bpp    = 32,
	.freq   = 60,

	.timing = {
		 .h_fp   = 48,
		 .h_bp   = 80,
		 .h_sw   = 32,
		 .v_fp   = 3,
		 .v_fpe  = 1,
		 .v_bp   = 17,
		 .v_bpe  = 1,
		 .v_sw   = 6,
	},

	.polarity = {
		 .rise_vclk		= 1,
		 .inv_hsync		= 1,
		 .inv_vsync		= 0,
		 .inv_vden		= 0,
	},

	.init_ldi = NULL,
};
/* VGA-1440X900 */
static struct s3cfb_lcd HDMI = {
	.width  = 1920,
	.height = 1080,
	.bpp    = 32,
	.freq   = 60,

	.timing = {
		 .h_fp   = 48,
		 .h_bp   = 80,
		 .h_sw   = 32,
		 .v_fp   = 3,
		 .v_fpe  = 1,
		 .v_bp   = 17,
		 .v_bpe  = 1,
		 .v_sw   = 6,
	},

	.polarity = {
		 .rise_vclk		= 1,
		 .inv_hsync		= 1,
		 .inv_vsync		= 0,
		 .inv_vden		= 0,
	},

	.init_ldi = NULL,
};
/* VGA-1280X1024 */
static struct s3cfb_lcd vga_1280_1024 = {
	.width	= 1280,
	.height	= 1024,
	.bpp	= 32,
	.freq	= 60,

	.timing = {
		.h_fp	= 48,
		.h_bp	= 248,
		.h_sw	= 112,
		.v_fp	= 1,
		.v_fpe	= 1,
		.v_bp	= 38,
		.v_bpe	= 1,
		.v_sw	= 3,
	},
	
	.polarity = {
		.rise_vclk      = 1,
		.inv_hsync      = 1,
		.inv_vsync      = 1,
		.inv_vden       = 0,
	},
	
	.init_ldi = NULL,
};

static struct {
	char * name;
	struct s3cfb_lcd * lcd;
} x4412_lcd_config[] = {
	{ "S70",		&wvga_s70},
	{ "W101",		&wsvga_w101},
	{ "W104",	&wsvga_w104},
	{ "GT70",	&wvga_gt70 },
	{ "GT101",	&wsvga_gt101 },
	{ "GS101",	&wsvga_gs101 },
	{ "H43",	&wsvga_h43 },
	{ "vga_1024_768",	&vga_1024_768},
		{ "HDMI",	&HDMI},
	{ "vga-1440x900",	&vga_1440_900},
	{ "vga-1280x1024",	&vga_1280_1024},
};

static unsigned char lcd_name[32] = "S70";
static int lcd_idx ;//= 0;
static int __init lcd_setup(char * str)
{
int i;
	for (i = 0; i < ARRAY_SIZE(x4412_lcd_config); i++) {
		if (!strcasecmp(x4412_lcd_config[i].name, str)) {
			lcd_idx = i;
			strcpy(lcd_name, str);
			break;
		}
	}
	return 1;
}
__setup("lcd=", lcd_setup);


void s3cfb_set_lcd_info(struct s3cfb_global *ctrl)
{
	struct s3cfb_lcd * lcd = x4412_lcd_config[0].lcd;
	int i;

	for(i = 0; i < ARRAY_SIZE(x4412_lcd_config); i++)
	{
		if(strcasecmp(x4412_lcd_config[i].name, lcd_name) == 0)
		{
			lcd = x4412_lcd_config[i].lcd;
			lcd_idx = i;
			break;
		}
	}
	ctrl->lcd = lcd;
	printk("lcd: select %s\r\n", lcd_name);
}
struct s3cfb_lcd *smdk4x12_get_lcd(void)
{
	return x4412_lcd_config[lcd_idx].lcd;
}

