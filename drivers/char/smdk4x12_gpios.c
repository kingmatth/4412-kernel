#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/types.h>
#include <linux/moduleparam.h>
#include <linux/slab.h>
#include <linux/ioctl.h>
#include <linux/cdev.h>
#include <linux/delay.h>
 
#include <linux/gpio.h>
#include <mach/gpio.h>
#include <plat/gpio-cfg.h>


#define DEVICE_NAME "gpios"

static int gpios[] = {
	EXYNOS4_GPX1(6),
	/*EXYNOS4_GPC1(0),
	EXYNOS4_GPD0(2),
	EXYNOS4_GPX3(5),
	EXYNOS4_GPX3(4),*/
	EXYNOS4_GPX1(7),
	EXYNOS4_GPZ(5),
	EXYNOS4_GPZ(6),
};

#define GPIO_NUM		ARRAY_SIZE(gpios)


static long smdk4x12_gpios_ioctl(struct file *filp, unsigned int cmd,
		unsigned long arg)
{
	if (arg > GPIO_NUM)
	{
		return -EINVAL;
	}
	//gps reset
	if (arg == 5)
	{
		gpio_direction_output(gpios[arg], 0);
		mdelay(10);
		gpio_direction_output(gpios[arg], 1);
		mdelay(500);
		gpio_direction_output(gpios[arg], 0);
		return 0;
	}
		
//printk("--------arg = %d, cmd = %d \n",arg,cmd);
	switch(cmd)
	{
		case 0:
			gpio_direction_output(gpios[arg], 0);
			return 0;
		case 1:
			gpio_direction_output(gpios[arg], 1);
			return 0;
			
		default:
			return -EINVAL;
	}
//printk("ioctl\n");

	return 0;
}

static struct file_operations smdk4x12_gpio_dev_fops = {
	.owner			= THIS_MODULE,
	.unlocked_ioctl	= smdk4x12_gpios_ioctl,
};

static struct miscdevice smdk4x12_gpio_dev = {
	.minor			= MISC_DYNAMIC_MINOR,
	.name			= DEVICE_NAME,
	.fops			= &smdk4x12_gpio_dev_fops,
};

static int __init smdk4x12_gpio_dev_init(void) {
	int ret;
	int i;

	for (i = 0; i < GPIO_NUM; i++) {
		ret = gpio_request(gpios[i], "GPIO");
		if (ret) {
			printk("%s: request GPIO %d for GPIO-API failed, ret = %d\n", DEVICE_NAME,
					gpios[i], ret);
			return ret;
		}

		s3c_gpio_cfgpin(gpios[i], S3C_GPIO_OUTPUT);
		gpio_set_value(gpios[i], 0);
	}

	ret = misc_register(&smdk4x12_gpio_dev);

	printk(DEVICE_NAME"\tinitialized\n");

	return ret;
}

static void __exit smdk4x12_gpio_dev_exit(void) {
	int i;

	for (i = 0; i < GPIO_NUM; i++) {
		gpio_free(gpios[i]);
	}

	misc_deregister(&smdk4x12_gpio_dev);
}

module_init(smdk4x12_gpio_dev_init);
module_exit(smdk4x12_gpio_dev_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Boardcon Inc.");

